struct timeval
{
  __time_t tv_sec;
  __suseconds_t tv_usec;
};

int main(int argc, char **argv) {
    VOS_UINT32 len;
    VOS_UINT8* buffer = malloc(1000000);
    struct timeval tv;
    gettimeofday(&tv, 0);
    VOS_INT64 time = (VOS_INT64)tv.tv_sec * 1000 + tv.tv_usec / 1000;
    c_MSG_LTLM_ADD_CA_REQ(buffer, len, atoi("10000"), atoi("1"));
    gettimeofday(&tv, 0);
    time = (VOS_INT64)tv.tv_sec * 1000 + tv.tv_usec / 1000 - time;
    printf("%ld\n", time);
}
