#!/bin/bash
d=$(dirname $0)
cd $d
p=$(pwd)
cd - > /dev/null
out=out
mkdir -p $out
function with_preprocess() {
	/usr/bin/time -f %e gcc -c -E -P $CFLAGS $1 -o $out/${1/.c/.i}
	/usr/bin/time -f %e $p/dctags -D 1 -u --c-kinds=+px-d-m --langmap=c:.c.i $out/${1/.c/.i} > $out/${1/.c/.cc}
}
function without_preprocess() {
	/usr/bin/time -f %e $p/dctags -D 1 -u --c-kinds=+px-d-m --langmap=c:.c.i $1 > $out/${1/.c/.cc}
}

function precompile() {
	/usr/bin/time -f %e gcc -c $CFLAGS $1
	with_preprocess $1
	/usr/bin/time -f %e awk -f $p/t.awk $out/${1/.c/.cc} 
	/usr/bin/time -f %e gcc -c $CFLAGS $out/${1/.c/.i.pu.c} -o $out/${1/.c/.o} >& $out/${1/.c/.log}
	cat $out/${1/.c/.log}
}
function all() {
	if [ "$1" == "ctags" ]; then
		CFLAGS="-I. -DHAVE_CONFIG_H -DDEBUG"
		files="ant.c args.c asm.c asp.c awk.c basic.c beta.c c.c cobol.c debug.c dosbatch.c eiffel.c entry.c erlang.c flex.c fortran.c get.c html.c jscript.c keyword.c lisp.c lregex.c lua.c main.c make.c matlab.c objc.c ocaml.c options.c parse.c pascal.c perl.c php.c python.c read.c readtags.c rexx.c routines.c ruby.c scheme.c sh.c slang.c sml.c sort.c sql.c strlist.c tcl.c tex.c verilog.c vhdl.c vim.c vstring.c yacc.c"
	elif [ "$1" == "vim" ]; then
		CFLAGS="-I. -DHAVE_CONFIG_H -Iproto -I/usr/local/include"
		files="blowfish.c buffer.c charset.c diff.c digraph.c edit.c eval.c ex_cmds2.c ex_cmds.c ex_docmd.c ex_eval.c ex_getln.c fileio.c fold.c getchar.c hardcopy.c hashtab.c if_cscope.c if_xcmdsrv.c main.c mark.c mbyte.c memfile.c memline.c menu.c message.c misc1.c misc2.c move.c netbeans.c normal.c ops.c option.c os_unix.c pathdef.c popupmnu.c quickfix.c regexp.c screen.c search.c sha256.c spell.c syntax.c tag.c term.c ui.c undo.c version.c window.c"
	fi
	for f in $files; do
		echo $f
		precompile $f
	done
}

if [ "$1" == "" ]; then
	all ctags
	exit
fi
if [ ! -f $1 ]; then
	all $1
else
	precompile $1
fi
