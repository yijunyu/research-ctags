/*
*   $Id$
*
*   Copyright (c) 1996-2002, Darren Hiebert
*
*   This source code is released for free distribution under the terms of the
*   GNU General Public License.
*
*   This module contains debugging functions.
*/

/*
*   INCLUDE FILES
*/
#include "general.h"  /* must always come first */

#include <ctype.h>
#include <stdarg.h>

#include "debug.h"
#include "options.h"
#include "read.h"

#include <string.h>
/*
*   FUNCTION DEFINITIONS
*/

#ifdef DEBUG

extern void lineBreak (void) {}  /* provides a line-specified break point */

extern void debugPrintf (
		const enum eDebugLevels level, const char *const format, ... )
{
	va_list ap;

	va_start (ap, format);
	if (debug (level))
		vprintf (format, ap);
	fflush (stdout);
	va_end (ap);
}

char *input_buffer = NULL;
unsigned int input_size = 1024;
int input_pos = 0;
#if 0
extern char *strcpy(char *dest, const char *src);
#endif
extern void free(void *ptr);
void input_getc(char c) {
	const char * old = NULL;
        if (input_pos == input_size && input_buffer != NULL) {
		input_size <<= 1;
		old = input_buffer;
		input_buffer = NULL;
	}
        if (input_buffer == NULL) {
		input_buffer = malloc(input_size);
		if (old != NULL) {
			(void) strcpy(input_buffer, old);
			free((void*) old);
		}
	}
	input_buffer [input_pos++] = c;
	input_buffer [input_pos] = 0;
}
void input_reset(const char *const kind, const char *const name, const char* const file) {
	if (name) {
		printf("%s\n==%s:%s:%s\n", input_buffer, kind, name, file);
		input_pos = 0;
        if (input_buffer == NULL) {
			const char * old = NULL;
			input_buffer = malloc(input_size);
			if (old != NULL) {
				(void) strcpy(input_buffer, old);
				free((void*) old);
			}
		}
		input_buffer[input_pos] = 0;
	}
}
static char name_buffer[1024];
static char kind_buffer[1024];
static char file_buffer[1024];
char *postponed_name = name_buffer;
char *postponed_kind = kind_buffer;
char *postponed_file = file_buffer;
void input_reset_skip_brace(const char *const kind, const char *const name, const char *const file) {
	if (name) {
		strcpy(postponed_name, name);
		strcpy(postponed_kind, kind);
		strcpy(postponed_file, file);
	}
}
extern void debugPutc (const int level, const int c)
{
	if (debug (level)  &&  c != EOF)
	{
	    if (c == STRING_SYMBOL)  printf ("\"string\"");
		else if (c == CHAR_SYMBOL)    printf ("'c'");
		else                          
#if 0
			putchar (c);
#else
			input_getc(c);
#endif
		fflush (stdout);
	}
}

extern void debugParseNest (const boolean increase, const unsigned int level)
{
	debugPrintf (DEBUG_PARSE, "<*%snesting:%d*>", increase ? "++" : "--", level);
}

extern void debugCppNest (const boolean begin, const unsigned int level)
{
	debugPrintf (DEBUG_CPP, "<*cpp:%s level %d*>", begin ? "begin":"end", level);
}

extern void debugCppIgnore (const boolean ignore)
{
	debugPrintf (DEBUG_CPP, "<*cpp:%s ignore*>", ignore ? "begin":"end");
}

extern void skipBraces (void);
extern void debugEntry (const tagEntryInfo *const tag)
{
	const char *const scope = tag->isFileScope ? "{fs}" : "";

	if (debug (DEBUG_PARSE))
	{
		printf ("<#%s%s:%s", scope, tag->kindName, tag->name);

		if (tag->extensionFields.scope [0] != NULL  &&
				tag->extensionFields.scope [1] != NULL)
			printf (" [%s:%s]", tag->extensionFields.scope [0],
					tag->extensionFields.scope [1]);

		if (Option.extensionFields.inheritance  &&
				tag->extensionFields.inheritance != NULL)
			printf (" [inherits:%s]", tag->extensionFields.inheritance);

		if (Option.extensionFields.fileScope &&
				tag->isFileScope && ! isHeaderFile ())
			printf (" [file:]");

		if (Option.extensionFields.access  &&
				tag->extensionFields.access != NULL)
			printf (" [access:%s]", tag->extensionFields.access);

		if (Option.extensionFields.implementation  &&
				tag->extensionFields.implementation != NULL)
			printf (" [imp:%s]", tag->extensionFields.implementation);

		if (Option.extensionFields.typeRef  &&
				tag->extensionFields.typeRef [0] != NULL  &&
				tag->extensionFields.typeRef [1] != NULL)
			printf (" [%s:%s]", tag->extensionFields.typeRef [0],
					tag->extensionFields.typeRef [1]);
		printf ("#>");
		fflush (stdout);
	}
        if (input_buffer && input_buffer[input_pos-1] == '{') {
		input_reset_skip_brace(tag->kindName, tag->name, tag->sourceFileName);
	} else {
		input_reset(tag->kindName, tag->name, tag->sourceFileName);
		if (strcmp(tag->kindName, "enumerator")!=0) {
			strcpy(postponed_name, "");
			strcpy(postponed_kind, "");
		} else if (strcmp(postponed_name, "")==0){
			input_reset_skip_brace(tag->extensionFields.scope [0], tag->extensionFields.scope [1], tag->sourceFileName);
		}
	}
}

#endif

/* vi:set tabstop=4 shiftwidth=4: */
