p=$(pwd)
make dctags
cp dctags ~/.local/bin/ctags
cd ~/abc/src
cp $p/callgraph.txt .
ctags -D 1 -u *.rs | awk -f $p/t.awk
cat *.pu.c
cd -
