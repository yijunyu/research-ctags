/*
*   $Id$
*
*   Copyright (c) 1998-2002, Darren Hiebert
*
*   This source code is released for free distribution under the terms of the
*   GNU General Public License.
*
*   External interface to keyword.c
*/
#ifndef _ID_H
#define _ID_H

/*
*   INCLUDE FILES
*/
#include "general.h"  /* must always come first */

#include "parse.h"

/*
*   FUNCTION PROTOTYPES
*/
extern void addID (const char *const string, int value);
extern int lookupID (const char *const string);
extern void freeIDTable (void);
#ifdef DEBUG
extern void printIDTable (void);
#endif
extern int analyzeTokenForID (vString *const name);

#endif  /* _ID_H */

/* vi:set tabstop=4 shiftwidth=4: */
