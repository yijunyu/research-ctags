BEGIN {
    i = 0
    k = 0
    lines = ""
}

/^==.*:.*/ {
    split($0, a, /==/)
    split(a[2], b, /:/)
    u = a[2]
    type = b[1]
    name = b[2]
    file = b[3]

    if (type == "enum" || type == "typedef" && match(lines, /enum/) > 0 && match(lines, /{/) > 0) {
        for (l = 0; l < k; l++) {
            if (pu[to_dep[l]] == "" && name!= to_dep[l]) {
                if (index(to_dep[l], ":") > 0) {
                    dep_order[nd] = to_dep[l] "->" name
		    nd = nd + 1
                    dep[to_dep[l] "->" name] = 1
                    split(to_dep[l], x, ":")
                    tags[name] = tags[name] " " x[1] ":" x[3]
                } else {
                    if (!dep["enumerator:" to_dep[l] ":" file "->" name]) {
                        dep_order[nd] = u "->" to_dep[l]
                        nd = nd + 1
                        dep[u "->" to_dep[l]] = 1
                        tags[name] = tags[name] " " type ":" file
                    }
                }
            }
        }
        for (m = 0; m < na; m++) {
            aliasname = aliases[m]
            alias[aliasname] = u
            pu["alias:" aliasname] = ""
            pu_order[i] = "alias:" aliasname
            i = i + 1
        }
        na = 0
        k = 0
        pu[u] = lines
        lines = ""
    } else if (type == "enumerator") {
        to_dep[k] = u
	k=k+1
        pu[u] = ""
    } else if (type == "externvar") {
        if (match(lines, "extern") <= 0 && match(lines, "struct") <= 0 || match(lines, ";") <= 0) {
            gsub(/,/, ";", lines)
            if (match(lines, "extern") > 0) {
                headlines = lines
                for (l = 0; l < k; l++) {
                    to_dep2[l] = to_dep[l]
                }
                to_k = k
                gsub(name, "<VAR>", headlines)
            } else if (match(lines, "struct") <= 0) {
                lines = headlines
                for (l = 0; l < to_k; l++) {
                    to_dep[l] = to_dep2[l]
                }
                k = to_k
                gsub("<VAR>", name, lines)
            }
        }
        for (l = 0; l < k; l++) {
            dep_order[nd] = u "->" to_dep[l]
            nd = nd + 1
            dep[u "->" to_dep[l]] = 1
        }
        k = 0
        pu[u] = lines
        lines = ""
    } else {
        for (l = 0; l < k; l++) {
            dep_order[nd] = u "->" to_dep[l]
	    nd = nd + 1
            dep[u "->" to_dep[l]] = 1
        }
        k = 0
        pu[u] = lines
        lines = ""
    }
    pu_order[i] = u
    i = i + 1
}

/^\+\+\+\+\+\+/ {
    split($0, a, /\+\+\+\+\+\+/)
    name = a[2]
    to_dep[k] = name
    k = k+1
}

/^===.*/ {
    split($0, a, /===/)
    name = a[2]
    to_dep[k] = name
    k = k + 1
    aliases[na] = name
    na = na+1
}

/^#/ {
    lines = lines "\n" $0
}

{
    if ($0!~ /^==.*:.*/ && $0!~ /^\+\+\+\+\+\+.*/ && $0!~ /^#.*/ && $0!~ /^===.*/ && $0!~ /^$/) {
        if (lines!= "") {
            lines = lines "\n" $0
        } else {
            lines = $0
        }
    }
}

END {
    if (lines!= "") {
        pu[pu_order[i - 1]] = pu[pu_order[i - 1]] "\n" lines
    }

    for (j = 0; j < i; j++) {
        u = pu_order[j]
        split(u, b, /:/)
        type = b[1]
        name = b[2]
        file = b[3]
        files[file] = 1
        tags[name] = tags[name] " " type ":" file
    }

    if (to_split)
        uid = 1

    for (j = 0; j < i; j++) {
        u = pu_order[j]
        split(u, b, /:/)
        type = b[1]
        name = b[2]
        file = b[3]
        if (type == "fn" || type == "function" || type == "variable") {
            if (to_split) {
                print("===========" u " ====== " uid " ==== " j)
                for (j1 = 0; j1 < j; j1++) {
                    u1 = pu_order[j1]
                    necessary[u1] = 0
                }
            }
            necessary[u] = 1
            if (to_split)
                uid = use_dependency(uid, j, u, dep, nd, dep_order, necessary, tags, alias, pu, pu_order, i);
        }
    }
    if (!to_split)
        uid = use_dependency(0, j, u, dep, nd, dep_order, necessary, tags, alias, pu, pu_order, i);
}

function use_dependency(uid, j, u, dep, nd, dep_order, necessary, tags, alias, pu, pu_order, i) {
    for (d in dep) {
        cloned_dep[d] = dep[d]
    }
    c = 1
    while (c > 0) {
        c = 0
        for (id = 0; id < nd; id++) {
            d = dep_order[id]
            if (cloned_dep[d]) {
                split(d, a, /->/)
                from = a[1]
                split(from, xx, /:/)
                name = xx[2]
                to = a[2]
                if (necessary[from] && to!= name) {
                    c = update_dependency(to, d, c, necessary, cloned_dep, tags);
                    if (alias[to]!= "")
                        c = update_dependency(alias[to], d, c, necessary, cloned_dep, tags)
                }
            }
        }
    }
    print_necessary_units(j + 1, pu_order, necessary, pu, uid++)
    return uid
}

function update_dependency(name, d, c, necessary, dep, tags) {
    n = split(tags[name], a)
    if (a[1] == "") return c;
    split(a[1], x, /:/)
    type = x[1]
    file = x[2]
    u = type ":" name ":" file
    if (!necessary[u]) {
       dep[d] = 0
       necessary[u] = 1
       return c + 1;
    }
    return c;
}

function convert_function_to_declaration(code) {
    code = "extern " code;
    n = split(code, a, "{");
    code = a[1] ";";
    return code;
}

function convert_variable_to_declaration(code) {
    if (code ~ /#pragma/) {
        n = split(code, x, /\n/);
        if (n > 2) {
            code = x[1] x[2] "\nextern ";
            for (xi = 3; xi <= n; xi++) {
                code = code x[xi] "\n";
            }
        }
    } else {
        code = "extern " code;
    }
    n = split(code, a, "=");
    code = a[1] ";";
    return code;
}

function print_necessary_units(i, pu_order, necessary, pu, uid) {
    c = 0;
    for (xi = 0; xi < i; xi++) {
        u = pu_order[xi]
        if (u!~ "enumerator:" && necessary[u]) c=c+1
    }
    k = 0;
    for (xj = 0; xj < i; xj++) {
        u = pu_order[xj]
        if (u!~ "enumerator:" && necessary[u]) {
            k=k+1
            code = pu[u]
            split(u, a, /:/)
            type = a[1]
            name = a[2]
            file = a[3]
            if (type == "variable" && k < c) {
                code = convert_variable_to_declaration(code);
            }
            if (type == "function" && k < c) {
                code = convert_function_to_declaration(code);
            }
            if (k <= c) {
                if (uid == 0)
                    print code > file ".pu.c"
                else
                    print code > file "_" uid ".pu.c"
            }
        }
    }
}
