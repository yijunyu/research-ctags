# debug = 1 necessary units by dependencies
# debug = 2 enum and typedef enum
# debug = 3 program unit names
# debug = 4 all dependencies
# debug = 5 aliases
# debug = 6 program unit text before computing necessary ones
# debug = 7 compilation units 
BEGIN {
  i = 0
  k = 0
  lines = ""
  lineno = 0
  if(system("[ -f callgraph.txt ]") == 0) {
	  while ((getline call < "callgraph.txt" )!=EOF) {
		  calls[call] = 1
	  }
  }
}

/^==.*:.*/ {
  split($0, a, /==/)
  split(a[2], b, /:/)
  u = a[2]
  type = b[1]
  name = b[2]
  file = b[3]
  if (type == "enum" || type == "typedef" && match(lines, /enum/) >0 && match(lines, /{/)>0 ) {
	for (l =0; l<k; l++) {
	  dep_order[nd++] = to_dep[l] "->" name
	  dep["enumerator:" to_dep[l] ":" file "->" name] = 1
							if (debug == 2)
								print "/*" to_dep[l] "->" name " added */"
	}
	for (m=0; m<na; m++) {
		aliasname = aliases[m]
		alias[aliasname] = u
		pu["alias:" aliasname] = ""
		pu_order[i++] = "alias:" aliasname
							if (debug == 5)
								print "/* alias " aliasname "*/"
	}
	na = 0;
	k = 0;
	pu[u] = lines
	lines = ""
  } else if (type == "enumerator") {
	to_dep[k++] = u
	pu[u] = ""
  } else if (type == "externvar") {
	if (match(lines, "extern") <= 0 && match(lines, "struct") <= 0 || match(lines, ";")<=0 ) {
		gsub(/,/, ";", lines) 
		if (match(lines, "extern")>0) {
			headlines = lines;
			for (l =0; l<k; l++) {
				to_dep2[l] = to_dep[l]
			}
			to_k = k
			gsub(name, "<VAR>", headlines)
		} else if (match(lines, "struct") <=0) {
			lines = headlines
			for (l =0; l<to_k; l++) {
				to_dep[l] = to_dep2[l]
			}
			k = to_k
			gsub("<VAR>", name, lines) 
		}
	} 
	for (l =0; l<k; l++) {
	  dep_order[nd++] = u "->" to_dep[l]
	  dep[u "->" to_dep[l]] = 1
	}
	k = 0;
	pu[u] = lines
	lines = ""
  } else {
	for (l =0; l<k; l++) {
	  dep_order[nd++] = u "->" to_dep[l]
	  dep[u "->" to_dep[l]] = 1
	}
	k = 0;
	pu[u] = lines
	lines = ""
  }
  pu_order[i++] = u
} 
/^\+\+\+\+\+\+/ {
  split($0, a, /\+\+\+\+\+\+/)
  name = a[2]
  to_dep[k++] = name
}
/^===.*/ {
  split($0, a, /===/)
  name = a[2]
  to_dep[k++] = name
  aliases[na++] = name
}
/^#/ {
  lines = lines "\n" $0;
}
{
  if ($0 !~ /^==.*:.*/ && $0 !~ /^\+\+\+\+\+\+.*/ && $0 !~ /^#.*/ && $0 !~ /^===.*/ && $0 !~ /^$/ ) {
	if (lines != "") {
		lines = lines "\n" $0;
	} else
		lines = $0;
  }
}
END {
  if (lines!="") {
       pu[pu_order[i-1]] = pu[pu_order[i-1]] "\n" lines
  }
							if (debug==3) {
								print "/*" 
								print_unit_names(i, pu_order)
								print "*/"
							}
							if (debug==4) {
								print "/*" 
								for (d in dep) print d
								print "*/"
							}
							if (no_code==1) {
								exit
							}
							if (debug==6) {
								print_units(i, pu_order, pu)
							}
  for (u in pu) {
	split(u, b, /:/)
	type = b[1]
	name = b[2]
	file = b[3]
	files[file]=1
	tags[name] = tags[name] " " type ":" file
  }
							if (debug==7) {
								for (f in files)
									print "/*" f "*/"
							}
							if (no_code==2) {
								exit
							}
  for (f in files) {
	c = 0;
        for (u in pu) {
		split(u, b, /:/)
		type = b[1]
		name = b[2]
		file = b[3]
		if (f == file && (type == "function" || type == "variable") && (length(calls)==0 || calls[name] == 1)) {
			necessary[u] = 1
			c++;
		}
	}
	#  print_necessary_units(i, pu_order, necessary, pu)
	while (c>0) {
								if (debug == 1) {
									print "/* " c " */"
								}
		c = 0;
		for (id=0; id<nd; id++) {
			d = dep_order[id]
			if (dep[d]) {
				split(d, a, /->/)
				from = a[1]
				to = a[2]
				if (necessary[from]) {
					c = update_dependency(f, to, d, c, necessary, dep);
					c = update_dependency(f, alias[to], d, c, necessary, dep)
				}
			}
		}
	}
	print_necessary_units(f, i, pu_order, necessary, pu)
							if (no_code==3) {
								exit
							}
   }
}

function update_dependency(f, name, d, c, necessary, dep) {
	n = split(tags[name], a)
	if( a[1]=="") return c;
	for (ai =1; ai<=n; ai++) {
		split(a[ai], x, /:/)
		type = x[1]
		file = x[2]
		u = type ":" name ":" file
		if (!necessary[u] && file == f) {
							if (debug)
								print "/*" d ": " u "*/"
			dep[d] = 0
			necessary[u] = 1
			return c + 1;
		}
	}
	return c;
}

function print_unit_names(i, pu_order) {
  for (j =0; j<i; j++) {
	u = pu_order[j]
	if (u !~ /enumerator /) 
		print u
  }
}

function print_units(i, pu_order, pu) {
  for (j =0; j<i; j++) {
	u = pu_order[j]
	if (u !~ /enumerator:/) 
							if (debug>0) {
								print "/**" u "*/\n" pu[u]
							} else
		print pu[u]
  }
}

function print_necessary_units(f, i, pu_order, necessary, pu) {
  for (j = 0; j< i; j++) {
	u = pu_order[j]
	if (u !~ "enumerator:" && necessary[u]) {
							if (debug>0) {
								print "/**" u "*/\n" pu[u]
							} else
		code = pu[u]
		split(u, a, /:/)
		type = a[1]
		name = a[2]
		file = a[3]
		if (file == f)
			print code > file ".pu.c"
	}
  }
}
